module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'prettier',
    'plugin:prettier/recommended',
    'plugin:vue/recommended'
  ],
  plugins: ['prettier'],
  rules: {
    'vue/html-self-closing': ['error', { html: { void: 'always' } }],
    'vue/singleline-html-element-content-newline': ['off'],
    'vue/max-attributes-per-line': ['off']
  }
}
