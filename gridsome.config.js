module.exports = {
  siteName: 'Eclectic Method',
  plugins: [
    { use: 'gridsome-plugin-svg' },
    // {
    //   use: 'gridsome-plugin-gtm',
    //   options: {
    //     id: 'GTM-W23J53R',
    //     enabled: process.env.NODE_ENV === 'production'
    //   }
    // },
    {
      use: '@gridsome/source-graphql',
      options: {
        url:
          'https://api-eu-central-1.graphcms.com/v2/ckm1rr0wem6ko01z39uxd9mp5/master',
        fieldName: 'gcms',
        typeName: 'gcmsTypes'
      }
    }
  ],
  css: {
    loaderOptions: {
      postcss: {
        plugins: [require('tailwindcss'), require('autoprefixer')]
      }
    }
  }
}
