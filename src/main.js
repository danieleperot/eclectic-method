import DOMPurify from 'dompurify'
import DefaultLayout from '~/layouts/Default'

import '@fontsource/roboto-mono/index.css'
import '@fontsource/archivo-black/index.css'
import '~/assets/style/main.css'

export const safeHtml = (el, { value }) => {
  el.innerHTML = DOMPurify.sanitize(value, { ADD_ATTR: ['target'] })
}

export default function (Vue, { head }) {
  Vue.component('Layout', DefaultLayout)
  Vue.directive('safe-html', safeHtml)

  head.script.push({ src: '/iubenda.js' })
  head.script.push({ src: '//cdn.iubenda.com/cs/iubenda_cs.js', async: true })
}
