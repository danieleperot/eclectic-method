const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: ['./src/**/*.vue'],
  theme: {
    extend: {
      screens: {
        xs: '420px',
        '3xl': '1600px'
      },
      colors: {
        black: '#1B1B1B',

        em: {
          blue: '#2C92D5',
          'gray-lighter': '#E6E6E6',
          'gray-light': '#848484',
          gray: '#535353',
          orange: '#FF7338'
        }
      },
      fontFamily: {
        mono: ['Roboto Mono', ...defaultTheme.fontFamily.mono],
        sans: ['Archivo Black', ...defaultTheme.fontFamily.sans]
      },
      minHeight: {
        'screen-4/5': '80vh',
        'screen-9/10': '90vh'
      },
      spacing: {
        22: '5.5rem'
      }
    }
  },

  variants: {
    borderWidth: ['hover', 'focus', 'responsive', 'last'],
    padding: ['hover', 'focus', 'responsive', 'last']
  }
}
